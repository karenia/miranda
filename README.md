# Miranda

![](res/img/logo_white.png)

This project is a full rewrite of [Malody's current website][malody_web]. Malody is a cross-platform music game emulator.

This project is still in early development.

[malody_web]: https://m.mugzone.net/

## Technologies

- `angular` as web framework (chosen due to its stability and comprehensiveness);
- `markdown-it` as markdown parser (chosen due to its extendability);
- ? as Wiki-like syntax parser.

## Sister project

_A rewrite of Malody's backend server is also planned. No detailed plan is avaliable for now._

## Contributing

We welcome all kinds of contribution to this project! Feel free to submit issues if you find any problem, even better if you know how to solve it and start a pull request!

## Developing

This project uses Angular CLI 8.0.2 to generate and serve files. Please follow Angular's official documents to contribute to this project.

## Licensing

MIT (c) 2019 Karenia Works.

Malody is a music game developed by Mugzone. This is an _official_ front-end project.

- IBM Plex Sans and Plex Mono fonts are made by IBM and licensed under OFL.
- Metropolis font is made by Chris M. Simpson and licensed under OFL.

---

The project has changed its name to "Miranda" at Jul. 3th, 2019.
