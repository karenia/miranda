import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { NotFoundComponent } from './pages/not-found/not-found.component'

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./pages/index/index.module').then(module => module.IndexModule)
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then(module => module.LoginModule)
  },
  // {
  //   path: 'register',
  //   loadChildren: () =>
  //     import('./pages/register/register.module').then(module => module.LoginModule)
  // },
  {
    path: 'u/:id',
    loadChildren: () =>
      import('./pages/user/user.module').then(module => module.UserModule),
    pathMatch: 'prefix'
  },
  {
    path: '**',
    component: NotFoundComponent
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
