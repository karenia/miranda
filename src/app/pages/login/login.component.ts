import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  Directive,
  Input
} from '@angular/core'
import { UserService } from 'src/app/services/user.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(private userService: UserService) {}

  ngOnInit() {}

  email: string = ''
  password: string = ''

  @ViewChild('email_el', { static: true }) emailElement: ElementRef
  @ViewChild('password_el', { static: true }) passwordElement: ElementRef

  login(): void {
    this.reportLoginError(null)

    // TODO: Validate user input (`email` is a proper email address, both fields are not empty)

    if (this.email.trim().length <= 0) {
      this.reportLoginError('email', 'Email must not be empty!')
      return
    }

    if (this.password.trim().length <= 0) {
      this.reportLoginError('password', 'Password must not be empty!')
      return
    }

    // TODO: Check input and redirect if possible (RxJS here I come!)
    this.userService.loginUser(this.email, this.password).subscribe({
      next: data => {
        // TODO: redirect to previous page if possible, else go to index
        console.log('Login successful')
      },
      error: err => this.reportLoginError('password', err)
    })
  }

  reportLoginError(
    warnItem: null | 'email' | 'password',
    warnMessage?: string
  ): void {
    if (warnItem != null) {
      console.warn(`An error occurred when logging in: %s`, warnMessage)
    }

    if (warnItem == 'email') {
      this.warnEmail = true
      this.passwordWarning = warnMessage
      this.emailElement.nativeElement.focus()
    } else if (warnItem == 'password') {
      this.warnPassword = true
      this.passwordWarning = warnMessage
      this.passwordElement.nativeElement.focus()
    } else {
      this.warnEmail = false
      this.warnPassword = false
      this.passwordWarning = null
    }
  }

  warnEmail: boolean = false
  warnPassword: boolean = false
  passwordWarning = null
}
