import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core'
import {
  mdiArrowDown,
  mdiChevronRight,
  mdiDiscord,
  mdiFacebook,
  mdiGithubCircle,
  mdiTwitter
} from '@mdi/js'
import {
  trigger,
  transition,
  state,
  style,
  animate,
  stagger,
  query
} from '@angular/animations'

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  animations: [
    trigger('blockDisplayTrigger', [
      state(
        'normal',
        style({
          opacity: 1,
          transform: 'translateY(0px)'
        })
      ),
      transition('void => *', [
        query('.block-fade-in', [
          style({
            opacity: 0,
            transform: 'translateY(10px)'
          }),
          stagger(
            50,
            animate(
              '250ms ease-in',
              style({
                opacity: 1,
                transform: 'translateY(0px)'
              })
            )
          )
        ])
      ])
    ])
  ]
})
export class IndexComponent implements OnInit, AfterViewInit {
  constructor() {}

  ngOnInit() {}

  @ViewChild('intro_screen', { static: true })
  introScreenContainer: HTMLDivElement

  ngAfterViewInit(): void {
    // this.introScreenContainer.style.height =
  }

  mdi_rightChevron = mdiChevronRight
  mdi_github = mdiGithubCircle
  mdi_twitter = mdiTwitter
  mdi_facebook = mdiFacebook
  mdi_discord = mdiDiscord
  mdi_downArrow = mdiArrowDown
}
