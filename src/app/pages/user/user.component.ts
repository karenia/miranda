import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'
import { switchMap, map } from 'rxjs/operators'
import { UserService } from 'src/app/services/user.service'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  private userid: number
  private isMe: boolean

  constructor(private route: ActivatedRoute, private userService: UserService) {
    this.route.paramMap
      .pipe(map(paramMap => paramMap.get('id')))
      .forEach(uid => {
        if (uid == 'me') this.userid = userService.userData.id
        else this.userid = parseInt(uid)

        console.log(this.userid)
      })
  }

  ngOnInit() {}
}
