import { Pipe, PipeTransform } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'

/**
 * A pipe that pipes trusted html
 *
 * idea stolen form https://medium.com/@AAlakkad/angular-2-display-html-without-sanitizing-filtering-17499024b079
 */
@Pipe({ name: 'trustedHtml' })
export class TrustedHtmlPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(content: string): any {
    return this.sanitizer.bypassSecurityTrustHtml(content)
  }
}
