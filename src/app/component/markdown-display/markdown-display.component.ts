import { Component, Input, OnInit } from '@angular/core'
import * as markdownit from 'markdown-it'

@Component({
  selector: 'markdown',
  templateUrl: './markdown-display.component.html',
  styleUrls: ['./markdown-display.component.scss']
})
export class MarkdownDisplayComponent implements OnInit {
  @Input('markdown')
  set markdownData(data: string) {
    let md = new markdownit()
    this.renderedHtml = md.render(data)
  }

  renderedHtml: string

  constructor() {}

  ngOnInit() {}
}
