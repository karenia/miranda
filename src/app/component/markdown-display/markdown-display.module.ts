import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { MarkdownDisplayComponent } from './markdown-display.component'
import { TrustedHtmlPipe } from './trusted-html.pipe'

@NgModule({
  declarations: [MarkdownDisplayComponent, TrustedHtmlPipe],
  imports: [CommonModule],
  exports: [MarkdownDisplayComponent, TrustedHtmlPipe]
})
export class MarkdownDisplayModule {}
