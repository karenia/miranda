import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core'
import { UserService } from 'src/app/services/user.service'
import { isPlatformBrowser } from '@angular/common'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  constructor(
    public userService: UserService,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {}

  static pageTopEpsilon = 5

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.scrollListener()
      addEventListener('scroll', this.scrollListener())
    }
  }

  scrollListener(): () => void {
    return () => {
      if (window.scrollY <= NavbarComponent.pageTopEpsilon)
        this.shouldNavbarBeSolid = false
      else if (this.shouldNavbarBeSolid == false)
        this.shouldNavbarBeSolid = true
    }
  }

  shouldNavbarBeSolid: boolean = false
}
