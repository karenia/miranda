import { Injectable, Inject, PLATFORM_ID } from '@angular/core'
import { Observable } from 'rxjs'
import { map, catchError } from 'rxjs/operators'
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http'
import { Md5 } from 'ts-md5/dist/md5'
import { isPlatformBrowser } from '@angular/common'

/**
 * The data retrieved from the login process
 */
interface LoginData {
  /**
   * Status code.
   *
   * - `0`: successful login;
   * - `-1`: wrong HTTP action (use POST)
   * - `-3`: wrong password or email;
   * - `-10`: no form data
   */
  code: number

  /**
   * Data to return if request is successful/
   */
  data?: {
    /** The username */
    username: string

    /** User grouping */
    group: Array<number>

    /** User ID */
    uid: number

    /** _unknown data_ (wild guess: 1 if the user is a "gold user", 0 otherwise) */
    gold: number

    /** _unknown data_ */
    tcp: string

    /** The link to this user's avatar. Empty if he doesn't have one */
    avatar: string

    /** The access token for this user. */
    key: string
  }
}

@Injectable({ providedIn: 'root' })
export class UserService {
  isLoggedIn: boolean = false

  userData: UserData = UserData.__DebugDefaultUserData()

  accessToken: string
  refreshToken: string

  constructor(
    private httpClient: HttpClient,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    let token: string = null
    // deal with ssr stuff - we cannot have access token in server
    if (isPlatformBrowser(this.platformId))
      token = window.localStorage.getItem('access-token')

    if (token) {
      this.accessToken = token
      // this.isLoggedIn = true
    }
  }

  loginUser(email: string, password: string) {
    // TODO: Ask backend to change the user API to a more secure one.
    // Hashing the password with MD5 is definitely not good for this kind of
    // logging-in stuff.

    console.log('Login request for user %s', email)

    let passwordHash = new Md5().appendAsciiStr(password).end(false) as string

    let postPayload = new FormData()
    postPayload.append('name', email)
    postPayload.append('psw', passwordHash)
    postPayload.append('h', '0')
    postPayload.append('v', '0)')

    return this.httpClient
      .post<LoginData>('http://m.mugzone.net/cgi/login', postPayload)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          throw new Error(err.message)
        }),
        map((val, index) => {
          if (val.code == 0) {
            // login success
            this.isLoggedIn = true
            this.accessToken = val.data.key
            this.userData = new UserData(val)
            console.log(
              '%cSuccessfully logged in for user %s',
              'color: green;',
              email
            )
            return this.userData
          } else {
            // login failed
            // TODO: report this incident to user via some callback
            console.log(
              '%cLogin failed for user %s, error code %s',
              'color: red;',
              email,
              val.code
            )

            switch (val.code) {
              case -1:
                throw new Error('Request error')
              case -3:
                throw new Error('Username does not match password')
              case -10:
                throw new Error('Wrong method')
              default:
                throw new Error(`Unknown error; code: ${val.code}`)
            }
          }
        })
      )
  }

  updateUserInfo() {}
}

export class UserData {
  public static get defaultAvatarUri(): string {
    return '/assets/img/nishiko_may.jpg'
  }

  public id: number
  public username: string
  public avatarUri: string

  constructor(rawData?: LoginData) {
    if (rawData) {
      this.id = rawData.data.uid
      this.username = rawData.data.username
      this.avatarUri = rawData.data.avatar
        ? '//cni.malody.cn/avatar/' + rawData.data.avatar
        : UserData.defaultAvatarUri
    }
  }

  public static __DebugDefaultUserData(): UserData {
    let data = new UserData()
    data.username = 'Nishiko'
    data.avatarUri = '/assets/img/nishiko_may.jpg'
    return data
  }
}
